
# Documentación del Proyecto

## Descripción General
Este script proporciona herramientas para la gestión de datos utilizando Chromadb. 

## Características

### Funciones de Embedding
- **create_embedding_function(name)**: Crea una función de embedding basada en el tipo especificado, como 'default' o 'sentence_transformer'.

### Gestión de Clientes y Bases de Datos
- **create_in_memory_client(path)**: Inicializa un cliente de base de datos en memoria.
- **create_server_http_client(path)**: Configura un cliente que se comunica con un servidor HTTP.

### Operaciones de Base de Datos
- **create_or_get_database(client, name, embedding_function)**: Obtiene una base de datos existente o crea una nueva con la función de embedding especificada.

### Manejo de Documentos
- **add_documents_to_collection(collection, documents, embeddings, metadatas, ids)**: Añade documentos a la colección especificada.
- **update_documents(collection, ids, documents, embeddings, metadatas)**: Actualiza documentos específicos dentro de una colección.
- **delete_documents(collection, ids, where)**: Elimina documentos de la colección basándose en IDs y condiciones opcionales.

### Consultas
- **query(collection, query_embeddings, query_texts, n_results, where, where_document)**: Ejecuta una consulta contra la colección y devuelve una lista de documentos.

## Ejemplo de Uso
```python
# Inicializar función de embedding y cliente
embedding_func = create_embedding_function("sentence_transformer")
client = create_in_memory_client("path_to_db")

# Crear o obtener colección de base de datos
collection = create_or_get_database(client, "mi_colección", embedding_func)

# Añadir documentos a la colección
add_documents_to_collection(collection, ["doc1", "doc2"], ["embed1", "embed2"], [{"info": "meta1"}, {"info": "meta2"}], ["id1", "id2"])
```

## Dependencias
Asegúrate de que todas las bibliotecas necesarias están instaladas, como `chromadb` para la gestión de bases de datos y las bibliotecas apropiadas para las funciones de embedding que piensas utilizar.

