

import chromadb
from chromadb.utils import embedding_functions
import subprocess

""" 
oooooooooooo                                    o8o                                           
`888'     `8                                    `"'                                           
 888         oooo  oooo  ooo. .oo.    .ooooo.  oooo   .ooooo.  ooo. .oo.    .ooooo.   .oooo.o 
 888oooo8    `888  `888  `888P"Y88b  d88' `"Y8 `888  d88' `88b `888P"Y88b  d88' `88b d88(  "8 
 888    "     888   888   888   888  888        888  888   888  888   888  888ooo888 `"Y88b.  
 888          888   888   888   888  888   .o8  888  888   888  888   888  888    .o o.  )88b 
o888o         `V88V"V8P' o888o o888o `Y8bod8P' o888o `Y8bod8P' o888o o888o `Y8bod8P' 8""888P' 
                                                                                              

Funciones creadas : 

- create_embedding_function                                                                                              
- create_embedding                                                                                              
- create_in_memory_client
- create_server_http_client
- create_database
- get_database
- create_or_get_database
- add_documents_to_collection
- update_documents
- upsert_documents
- delete_documents
- get_documents
- query
- delete_database
                                                                                     

"""

# Create_embedding_function 
# Funcion que crea una funcion de embeddings, se le puede pasar el nombre de la funcion de embeddings
# por ejemplo "default" o "sentence_transformer"
# retorna una funcion de embeddings

def create_embedding_function(name):
    if name == "default":
        return embedding_functions.DefaultEmbeddingFunction()
    elif name == "sentence_transformer":
        return embedding_functions.SentenceTransformerEmbeddingFunction(model_name="all-MiniLM-L6-v2")
    else:
        raise ValueError("Invalid embedding function")
    
# Create_embedding
# Funcion que crea un embedding
# Se le pasa un string y una funcion de embeddings
# retorna un embedding




def create_embedding(embedding_function, str):
    return embedding_function([str])
    
    # embedding = embedding_function([str])    
    # return embedding[0]
    


# Create_client
# Funcion que crea un cliente para la base de datos
# Se le pasa el path de la base de datos
# retorna un cliente

def create_in_memory_client(path):
    client = chromadb.PersistentClient(path=path)
    return client

def create_server_http_client(path):
    subprocess.run(["chroma", "run", "--path", path])
    client = chromadb.HttpClient(host='localhost', port=8000)
    return client

# Solo uno de estos puede ser ejecutado.





# Create_database a Create_or_get_database
# Funcion que crea una base de datos
# Se le pasa un cliente, el nombre de la base de datos y una funcion de embeddings
# retorna una coleccion


def create_database(client, name, embedding_function):
    return client.create_collection(name=name, embedding_function=embedding_function)
    
def get_database(client, name, embedding_function):
    return client.get_collection(name=name, embedding_function=embedding_function)
    
def create_or_get_database(client, name, embedding_function):
    return client.get_or_create_collection(name=name, embedding_function=embedding_function)


# Add_documents_to_collection
# Funcion que añade documentos a una coleccion
# Se le pasa una coleccion y los documentos, embeddings, metadatas, ids
# retorna None


def add_documents_to_collection(collection, documents=None, embeddings=None, metadatas=None, ids=None):
    collection.add(documents=documents, embeddings=embeddings, metadatas=metadatas, ids=ids)

# Update_documents
# Funcion que actualiza documentos en una coleccion
# Se le pasa una coleccion, los ids, documentos, embeddings, metadatas
# retorna None
    

def update_documents(collection, ids, documents=None, embeddings=None, metadatas=None):
    collection.update(ids=ids, documents=documents, embeddings=embeddings, metadatas=metadatas)

# Upsert_documents
# Funcion que actualiza o inserta documentos en una coleccion
# Se le pasa una coleccion, los ids, documentos, embeddings, metadatas
# retorna None

    
def upsert_documents(collection, ids, documents=None, embeddings=None, metadatas=None):
    collection.upsert(ids=ids, documents=documents, embeddings=embeddings, metadatas=metadatas)

# Delete_documents
# Funcion que elimina documentos en una coleccion
# Se le pasa una coleccion, los ids, y opcionalmente un where
# retorna None
    
def delete_documents(collection, ids, where=None):
    collection.delete(ids=ids, where=where)

# Get_documents
# Funcion que retorna documentos en una coleccion
# Se le pasa una coleccion, los ids, y opcionalmente un where y un include
# retorna una lista de documentos

    
def get_documents(collection, ids=None, where=None):
    return collection.get(ids=ids, where=where)

# Query
# Funcion que realiza una query en una coleccion
# Se le pasa una coleccion, los query_embeddings, query_texts, n_results, y opcionalmente un where, where_document, include
# retorna una lista de documentos

def query(collection, query_embeddings=None, query_texts=None, n_results=10, where=None, where_document=None):
    return collection.query(query_embeddings=query_embeddings, query_texts=query_texts, n_results=n_results, where=where, where_document=where_document)

# Delete_database
# Funcion que elimina una coleccion
# Se le pasa un cliente y el nombre de la coleccion
# retorna None

def delete_database(client, name):
    client.delete_collection(name=name)
    
    
    
    




#########################################################




