from metodos_db import * 

    """
    
    
ooooooooooooo                        .   
8'   888   `8                      .o8   
     888       .ooooo.   .oooo.o .o888oo 
     888      d88' `88b d88(  "8   888   
     888      888ooo888 `"Y88b.    888   
     888      888    .o o.  )88b   888 . 
    o888o     `Y8bod8P' 8""888P'   "888" 
                                        
      
    """






    
    
def print_status_message(message):
    border = "#" * (len(message) + 6)  # Aumentamos el tamaño del borde para mejorar la presentación
    print(f"{'#' * (len(border) - 2)}")
    print(f"#{' ' * (len(border) - 4)}#")  # Línea vacía antes del mensaje
    print(f"#  {message}  #")  # Añadimos espacios adicionales para centrar el mensaje
    print(f"#{' ' * (len(border) - 4)}#")  # Línea vacía después del mensaje
    print(f"{'#' * (len(border) - 2)}")


print_status_message("Creando funcion de embeddings")
sentence_transformer = create_embedding_function("sentence_transformer")

print_status_message("Creando cliente")
cliente = create_in_memory_client("datos")

print_status_message("Creando base de datos")
datos_collection = create_database(cliente, "datos", sentence_transformer)

print("Añadiendo documentos a la coleccion")

print_status_message("Añadiendo documentos a la colección")
add_documents_to_collection(
    datos_collection,
    documents=["primer documento", "segundo documento", "tercer documento"],
    embeddings=[
        create_embedding(sentence_transformer, "primer documento")[0],  # Ajuste aquí
        create_embedding(sentence_transformer, "segundo documento")[0],  # Ajuste aquí
        create_embedding(sentence_transformer, "tercer documento")[0]    # Ajuste aquí
    ],
    metadatas=[
        {"numero": 1, "palabras": 2},
        {"numero": 2, "palabras": 2},
        {"numero": 3, "palabras": 2}
    ],
    ids=["primer_id", "segundo_id", "tercer_id"]
)

# add_documents_to_collection(datos_collection, documents=["primer documento", "segundo documento", "tercer documento"], embeddings


print("Obteniendo documentos de la coleccion")
print(get_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"]))

print("Query en la coleccion")
print(query(datos_collection, query_texts=["primer documento"], n_results=3))

print("Actualizando documentos de la coleccion")
update_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"], documents=["primer documento actualizado", "segundo documento actualizado", "tercer documento actualizado"])

print("Eliminando documentos de la coleccion")
delete_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"], where={"palabras": 2})

print("Eliminando la coleccion")
delete_database(cliente, "datos")